---
layout: page
title: Big Data Bootcamp
description: Georgia Tech Big Data Bootcamp training material
navigation:
  section: [0]
---

# Welcome

Welcome to the Big Data Bootcamp. This training material is has been developed by [Sunlab](http://www.sunlab.org/) and [Polo Club](http://poloclub.gatech.edu/). By the end of the training, you will learn about the big data tools that are part of the [Hadoop](http://hadoop.apache.org) and [Spark](http://spark.apache.org) ecosystems. 

The training material [sample data](data) is for healthcare applications, but you can adapt what you learned to other domains. There is no requirement of healthcare background knowledge.

To get started, please [**setup the learning environment**](environment) first.

# Content Summary
Content of the training material is divided into two chapters **Hadoop** and **Spark**.

## [Hadoop Ecosystem](hadoop)
1. [HDFS Basics](hdfs-basic)
1. [MapReduce Basics](mapreduce-basic)
2. [Hadoop HBase](hadoop-hbase)
3. [Hadoop Streaming](hadoop-streaming)
4. [Hadoop Pig](hadoop-pig)
5. [Hadoop Hive](hadoop-hive)

## [Spark Ecosystem](spark)
1. [Scala Basics](scala-basic)
2. [Spark Basics](spark-basic)
3. [Spark SQL](spark-sql)
4. [Spark Application](spark-application)
5. [Spark MLlib](spark-mllib)
6. [Spark GraphX](spark-graphx)


<div class="text-center col-md-12"><a href="hadoop/" class="btn btn-lg btn-info">Start Learning<span class="glyphicon glyphicon-export"></span></a></div>

